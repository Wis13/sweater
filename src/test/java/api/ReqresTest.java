package api;

import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import java.time.Clock;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class ReqresTest {
    private final static String URL = "https://reqres.in";

    @Test
    @Description("Test API contain avatar number id")
    public void checkAvatarAndIdTest() {
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec200());
        List<UserData> users = given()
                .when()
                .get("/api/users?page=2")
                .then().log().all()
                .extract().body().jsonPath().getList("data", UserData.class);

        users.forEach(x -> Assert.assertTrue(x.getAvatar().contains(x.getId().toString())));
        Assert.assertTrue(users.stream().allMatch(x -> x.getEmail().endsWith("@reqres.in")));

        List<String> avatars = users.stream().map(UserData::getAvatar).collect(Collectors.toList());
        List<String> ids = users.stream().map(x -> x.getId().toString()).collect(Collectors.toList());

        for(int i = 0; i < avatars.size(); i++ ){
            Assert.assertTrue(avatars.get(i).contains(ids.get(i)));
        }
    }

    @Test
    @Description("Test login with valid credentials")
    public void successRegTest(){
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec200());
        Integer id = 4;
        String token = "QpwL5tke4Pnpja7X4";
        Register user = new Register("eve.holt@reqres.in", "pistol");
        SuccessReg successReg = given()
                .body(user)
                .when()
                .post("/api/register")
                .then().log().all()
                .extract().as(SuccessReg.class);

        Assert.assertNotNull(successReg.getId());
        Assert.assertNotNull(successReg.getToken());
        Assert.assertEquals(id, successReg.getId());
        Assert.assertEquals(token, successReg.getToken());
    }

    @Test
    @Description("Test login with invalid credentials")
    public void unSuccessRegTest(){
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpecErr400());
        Register user = new Register("sydney@fife", "");
        UnSuccessReg unSuccessReg = given()
                .body(user)
                .when()
                .post("/api/register")
                .then().log().all()
                .extract().as(UnSuccessReg.class);
        Assert.assertEquals("Missing password", unSuccessReg.getError());

    }

    @Test
    @Description("Test return sorted years")
    public void sortedYearsTest(){
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec200());
        List<ColorsData> colorsData = given()
                .when()
                .get("/api/unknown")
                .then().log().all()
                .extract().body().jsonPath().getList("data", ColorsData.class);

        List<Integer> years = colorsData.stream().map(ColorsData::getYear).collect(Collectors.toList());
        List<Integer> sortedYears = years.stream().sorted().collect(Collectors.toList());

        Assert.assertEquals(sortedYears, years);

    }

    @Test
    @Description("Test delete user")
    public void deleteUserTest(){
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpecUnique(204));
        given()
                .when()
                .delete("/api/users?page=2")
                .then().log().all();

    }

    @Ignore
    @Test
    @Description("Test assert server time")
    public void getServerTime(){
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec200());
            UserTime user = new UserTime("morpheus", "zion resident");
            UserTimeResponse response = given()
                    .body(user)
                    .when()
                    .put("/api/users/2")
                    .then().extract().as(UserTimeResponse.class);
        String regex = "(.{10})$";
        String regex2 = "(.{13})$";
        String currentTime = Clock.systemUTC().instant().toString().replaceAll(regex2, "");
        Assert.assertEquals(currentTime, response.getUpdatedAt().replaceAll(regex,""));

    }

    @Ignore
    @Test
    @Description("Test assert server time 2")
    public void getServer2Time(){
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpec200());
        UserTimeResponse user = new UserTimeResponse();
        user.setName("morpheus");
        user.setJob("zion resident");
        HashMap<String, String> req = new HashMap<>();
            req.put("name", user.getName());
            req.put("job", user.getJob());
        UserTimeResponse response = given()
                .body(req)
                .when()
                .patch("/api/users/2")
                .then().extract().as(UserTimeResponse.class);
        String regex = "(.{10})$";
        String regex2 = "(.{13})$";
        String currentTime = Clock.systemUTC().instant().toString().replaceAll(regex2, "");
        Assert.assertEquals(currentTime, response.getUpdatedAt().replaceAll(regex,""));


    }
}
