package api;

public class UserTimeResponse {
    private String updatedAt;
    private String name;
    private String job;

    public UserTimeResponse() {
    }

    public UserTimeResponse(String updatedAt, String name, String job) {
        this.updatedAt = updatedAt;
        this.name = name;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }
}
